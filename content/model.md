# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt en sevärdhet och varje kolumn motsvarar en egenskap för den beskrivna sevärdheten. 16 attribut är definierade, där de första 6 är obligatoriska. Speciellt viktigt är att man anger [type](#type) av sevärdhet för att kunna underlätta återanvändning av specifika sevärdheter.

<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.


</div>

<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**id**](#id)|1|text|**Obligatoriskt** - Ange identifierare för sevärdheten.|
|**name**|1|text|**Obligatoriskt** - Ange namnet på sevärdheten.|
|[**source**](#source)|1|[heltal](#heltal)|**Obligatoriskt** - Ange organisationsnummret utan mellanslag eller bindesstreck för organisationen som står bakom sevärdheten.|
|[**type**](#type)|1|[type](#type)|**Obligatoriskt** - Ange sevärdhetens typ.|
|[**latitude**](#latitude)|1|[decimal](#decimal)|**Obligatoriskt** - Latitude anges per format enligt WGS84.|
|[**longitude**](#longitude)|1|[decimal](#decimal)|**Obligatoriskt** - Longitude anges per format enligt WGS84.|
|description|0..1|text|Ange ytterligare beskrivning av sevärdheten.|
|openinghours|0..1|text|Ange ytterligare beskrivning av öppettiderna som exempelvis vanligt förekommande öppettider.|
|opens|0..1|[time](#time)|Ange när sevärdheten öppnar.|
|closes|0..1|[time](#time)|Ange när sevärdheten stänger.|
|[street](#street)|0..1|text|Ange gatuadress för sevärdheten.|
|[postalcode](#postalcode)|0..1|[heltal](#heltal)|Ange postnummer för sevärdheten.|
|[city](#city)|0..1|text|Ange postort för sevärdheten.|
|[email](#email)|0..1|text|E-postadress för vidare kontakt, anges med gemener och med @ som avdelare.|
|[URL](#url)|0..1|[URL](#url)|Ingångssida för mer information om sevärdheten.|
|[image](#image)|0..*|[URL](#url)|Länk till bild på sevärdheten.|

</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

En länk till en webbsida där sevärdheten presenteras hos den lokala myndigheten eller organisation.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

### time 

En tidpunkt som återkommer på flera dagar i formen hh:mm:ss[Z|(+|-)hh:mm] (se XML-schema för detaljer)[http://www.w3.org/TR/xmlschema-2/#time].

hh:mm:ss eller hh:mm 

Exempel - en sevärdhet som öppnar kl 17:00. 

<div>

17:00

</div>

## Förtydligande av attribut

### **id**

Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

Ange identfierare för sevärdheten. 

### source

Ange organisationsnummret utan mellanslag eller bindesstreck för organisationen. Exempel för Gullspångs kommun: **2120001637**.

### type

Detta attribut anger typen av sevärdhet, observera att ni endast ska använda texten "Park" och inte de utåtgående länkarna. Länkarna är till för att underlätta förståelsen kring olika typer av sevärdheter. Då typerna hämtas från klasser i [schema.org](https://schema.org/) kan man ge förslag på ytterligare typer som bör finnas med i nästa version av specifikationen.

[LandmarksOrHistoricalBuildings](http://schema.org/LandmarksOrHistoricalBuildings)
<br>

[Park](http://schema.org/Park)
<br>

[Bridge](http://schema.org/Bridge)
<br>

[Church](http://schema.org/Church)
<br>

[Mountain](http://schema.org/Mountain)
<br>

[Waterfall](http://schema.org/Waterfall)
<br>

[RiverBodyOfWater](http://schema.org/RiverBodyOfWater)
<br>

[LakeBodyOfWater](http://schema.org/LakeBodyOfWater)
<br>

[Canal](http://schema.org/Canal)
<br>

[Museum](http://schema.org/Museum)
<br>

[Library](http://schema.org/Library)
<br>

### latitude

WGS84 är den standard som det amerikanska systemet GPS använder för att beskriva en latitud på jordklotets yta. GPS används av många kartapplikationer. Latitud anges med ett heltal följt av en decimalpunkt “.” och 1 till 8 decimaler. Exempelvis “61.21657”. En angivelse av latitud som befinner sig på jordens södra hemisfär anges med negativt tal. Exempelvis “-53.78589”. Om koordinatens inledande heltal är noll, skall alltid nollan anges. 

### longitude

Longitud anges med ett heltal följt av en decimalpunkt följt av 1 till 8 decimaler. Exempelvis “88.40901”. En longitud som ligger väster om WGS84-systemets meridian, anges med negativt tal, Exempelvis: “-0.158101”. Om koordinatens inledande heltal är noll, skall nollan alltid anges.


### street

Sevärdhetens adress.

### postalcode

Postnummer. Ange inte mellanslag. 

### city

Postort där sevärdheten finns.

### email

Funktionsadress till den organisation som ansvarar för sevärdheten. Ange ej det inledande URI schemat mailto: eller HTML-koder i uttrycket. Exempel: example@domain.se

### image

Länk till bild. Ange inledande schemat https:// eller http:// 



 





# Exempel i JSON

Samma entitet som i Appendix A, fast nu som JSON.

```json
{
    "id":  "4323-xx",
    "name":  "Högsåsen",
    "source": "2120001637",
    "type": "Park",
    "latitude":  "58.399237",
    "longitude":  "15.417352",
    "description":  "Parken ligger nära vattnet och har flera lekplatser samt ett utegym precis intill.",
    "openinghours":  "Öppet alla dagar",
    "opens":  "06:00",
    "closes":  "22:00",
    "street":  "Storgatan 4",
    "postalcode":  "99999",
    "city":  "Gullspång",
    "email":  "info@gullspång.se",
    "URL":  "https://www.vastsverige.com/gullspang/produkter/Hogsasen-endelavTiveden/?site=546",
    "image":  "",      
}
```

## Exempel i CSV, kommaseparerad

<div class="example csvtext">
id,name,source,type,latitude,longitude,description,openinghours,opens,closes,street,postalcode,city,email,URL,image
<br>
4323-xx,Högsåsen,2120001637,Park,58.399237,15.417352,Parken ligger nära vattnet och har flera lekplatser samt ett utegym precis intill.,Öppet alla dagar,06:00,22:00,Storgatan 4,99999,Gullspång,info@gullspång.se,https://www.vastsverige.com/gullspang/produkter/Hogsasen-endelavTiveden/?site=546,,
</div>

